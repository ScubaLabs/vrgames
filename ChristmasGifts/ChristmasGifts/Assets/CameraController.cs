﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {
	public float speed = 3.5f;
	public GameObject santa;
	// Use this for initialization
	void Start () {
		
	}

	// Update is called once per frame
	void Update () {
		// Make Santa move.
		santa.transform.position += this.transform.forward * speed * Time.deltaTime;
	}
}
