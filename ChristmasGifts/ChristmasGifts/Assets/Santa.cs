﻿using UnityEngine;
using System.Collections;
using System;

public class Santa : MonoBehaviour {


	public float throwingForce = 300f;
	public GameObject giftPrefab;
	public int score = 0;

	// Use this for initialization
	void Start () {
		Input.gyro.enabled = true;
	}
	
	// Update is called once per frame
	void Update () {
		

		// Throwing gifts.
		if (GvrViewer.Instance.Triggered) {
			GameObject giftObject = Instantiate (giftPrefab);
			giftObject.transform.position = this.transform.position;
			giftObject.GetComponent<Rigidbody> ().AddForce (this.transform.forward * throwingForce);

			Gift gift = giftObject.GetComponent<Gift> ();
			gift.onHitHouse = () => {
				score += 100;
			};
		}
	}
}
