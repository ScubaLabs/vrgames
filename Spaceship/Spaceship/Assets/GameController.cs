﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {

	public CameraContainer container;
	public GameObject[] asteroidPrefabs;
	public TextMesh scoreText;

	public float spawnDistance = 1f;
	public float spawnIncrement = 1f;

	private float spawnPointer = 0;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (container.transform.position.z > spawnPointer) {
			Debug.Log (container.transform.position.z);
			GameObject asteroidPrefab = asteroidPrefabs[Random.Range(0, asteroidPrefabs.Length)];
			GameObject asteroidObject = Instantiate (asteroidPrefab);

			asteroidObject.transform.position = new Vector3 (
				Random.Range(-2.3f, 2.3f),
				Random.Range(-2.3f, 2.3f),
				container.transform.position.z + spawnDistance
			);
			Debug.Log (asteroidObject.transform.position.z);
			spawnPointer = container.transform.position.z + spawnIncrement;
		}

		int playerScore = Mathf.RoundToInt (container.transform.position.z);
		if (playerScore < 0) {
			playerScore = 0;
		}
		scoreText.text = "Score: " + playerScore;
	}
}
