﻿using UnityEngine;
using System.Collections;

public class Wall : MonoBehaviour {

	public CameraContainer container;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (container.transform.position.z > 5) {
			transform.position = new Vector3 (
				transform.position.x,
				transform.position.y,
				container.transform.position.z
			);
		}
	}
}
