﻿using UnityEngine;
using System.Collections;

public class Bird : MonoBehaviour {


	public bool dead = false;
	public float jumpForce = 4f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (dead == true) {
			GetComponent<Rigidbody> ().useGravity = false;
			GetComponent<Rigidbody> ().velocity = Vector3.zero;
			return;
		}
		if (GvrViewer.Instance.Triggered || Input.GetButtonDown("Fire1")) {
			GetComponent<Rigidbody>().velocity = new Vector3 (
				0,
				jumpForce,
				GetComponent<Rigidbody>().velocity.z
			);
		}


		// Make bird jump

	}

	void OnTriggerEnter (Collider otherCollider) {
		if (otherCollider.tag == "Obstacle") {
			dead = true;
		}
	}
}
