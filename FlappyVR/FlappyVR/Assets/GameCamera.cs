﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameCamera : MonoBehaviour {
	public float speed = 3.5f;
	public GameObject Bird;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		GameObject gameObject;
	
		// Move forward
		Bird.GetComponent<Rigidbody>().velocity = new Vector3 (
			0,
			Bird.GetComponent<Rigidbody>().velocity.y,
			speed
		);

	}
}
