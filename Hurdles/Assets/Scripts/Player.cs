﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
	public float maximuSpeed = 5.5f;
	public float jumpingForce = 500f;
	public float jumpingCooldown = 1.5f;
	public float jumpingTimer = 0f;
	public float acceleration = 1f;
	public float speed = 0f;
	public bool reachedFinishedLine = false;
	// Use this for initialization
	void Start ()
	{
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		//Move player forward

		speed += acceleration * Time.deltaTime;
		if (speed > maximuSpeed) {
			speed = maximuSpeed;
		}

		transform.position += speed * Vector3.forward * Time.deltaTime;
		//Move player jump
		jumpingTimer -= Time.deltaTime;
		if (GvrViewer.Instance.Triggered||Input.GetButtonDown("Fire1")) {
			Debug.Log ("Jumping");
			if (jumpingTimer <= 0f) {
				jumpingTimer = jumpingCooldown;
				GetComponent<Rigidbody> ().AddForce (Vector3.up * jumpingForce);
			}
		}
	}

	void OnTriggerEnter (Collider collider)
	{
		if (collider.tag.Equals ("Obstacle")) {
			speed *= 0.3f;
		}
		if (collider.tag.Equals ("Finish")) {
			reachedFinishedLine = true;
		}
	}
}
