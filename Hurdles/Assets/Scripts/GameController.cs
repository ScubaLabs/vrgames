﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;	

public class GameController : MonoBehaviour {
	public TextMesh infoText;
	private float gameTimer=0f;
	private float restartTimer=3f;
	public Player player;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (player.reachedFinishedLine == false) {
			gameTimer += Time.deltaTime;
			infoText.text = "Avoid the obstacles\nPress button to jump\n Time: " + Mathf.Floor (gameTimer) + " seconds";
		} else {
			infoText.text = "Game Over \n Your Time: " + Mathf.Floor (gameTimer) + " seconds";
			restartTimer -= Time.deltaTime;
			if (restartTimer <= 0) {
				SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
			}

		}
	}
}
