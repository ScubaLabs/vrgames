﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

	public Kart kart;
	public TextMesh infoText;

	private float timer;
	private float recordTime = 999;
	public float speed = 1f;
	public GameObject kartModel;
	private int kartLap;
	private float restartTimer = 2f;

	// Use this for initialization
	void Start () {
		kartLap = 0;
	}
	
	// Update is called once per frame
	void Update () {
		timer += Time.deltaTime;

		if (kart.CurrentLap > kartLap) {
			kartLap = kart.CurrentLap;

			if (timer < recordTime) {
				recordTime = timer;
			}

			timer = 0;
		}

		infoText.text = "Time: " + Mathf.Floor(timer);
		if (recordTime < 999) {
			infoText.text += "\nRecord: " + Mathf.Floor(recordTime);
		}

//		kartModel.transform.eulerAngles = new Vector3 (
//			-90,
//			transform.eulerAngles.y,
//			transform.eulerAngles.z
//		);
		//Debug.Log (kartModel.transform.position);
		Vector3 movementDirection = new Vector3 (
			kartModel.transform.forward.x,
			0,
			kartModel.transform.forward.z
		);
		transform.position += movementDirection.normalized * speed * Time.deltaTime;
		if (kart.isGameOver) {
			finishGame ();
		}
	}
	void finishGame(){
		infoText.text = "Game Over! "+"Your Time: " + Mathf.Floor(timer);
		restartTimer -= Time.deltaTime;
		if (restartTimer <= 0f) {
			SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
		}
	}
}
