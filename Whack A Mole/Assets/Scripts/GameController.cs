﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {
	public GameObject moleContainer;
	private Mole[] moles;
	private float spawnTimer = 0f;
	public float spawnDecrement=0.1f;
	public float spawnDuration = 1.5f;
	public float minimumSpawnDuration = 1.5f;
	public float gameTimer = 50f;
	public TextMesh infoText;
	public Player player;
	private float resetTimer = 3f;

	// Use this for initialization
	void Start () {
		moles = moleContainer.GetComponentsInChildren<Mole> ();

	}
	
	// Update is called once per frame
	void Update () {
		
		if (gameTimer > 0f) {
			infoText.text = "Hit all the moles!\nTime: " + Mathf.Floor (gameTimer) + "\n Player Score : " + player.score;
			spawnTimer -= Time.deltaTime;
			if (spawnTimer <= 0f) {
				moles [Random.Range (0, moles.Length)].Rise ();
				spawnDuration -= spawnDecrement;
				if (spawnDuration < minimumSpawnDuration) {
					spawnDuration = minimumSpawnDuration;
				}
				spawnTimer = spawnDuration;
			}
			gameTimer -= Time.deltaTime;
		} else {
			infoText.text = "Game Over. \n Your score : " + Mathf.Floor (player.score);
			resetTimer -= Time.deltaTime;
			if (resetTimer <= 0f) {
				SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
			}

		}
	}
}
